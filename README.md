# 微部落-micro

#### 介绍
基于Vue+SpringBoot的微部落博客类系统，可实现发文聊天回复评论点赞文章审核等功能，包括前后台。

#### 功能演示
1.注册登录界面

![输入图片说明](https://foruda.gitee.com/images/1690251868330118674/bb077ba8_10270245.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1690251889992863666/5b50c839_10270245.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1690251901383996812/b4b09174_10270245.png "屏幕截图")
2.主页

![输入图片说明](https://foruda.gitee.com/images/1690251915184379705/a5874aae_10270245.png "屏幕截图")
3.文章详情

![输入图片说明](https://foruda.gitee.com/images/1690251931990821111/d184f0b0_10270245.png "屏幕截图")
4.新增文章

![输入图片说明](https://foruda.gitee.com/images/1690251942828204978/78308ff8_10270245.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1690251951078457387/cecc46bc_10270245.png "屏幕截图")
5.评论和回复

![输入图片说明](https://foruda.gitee.com/images/1690252608325555057/b747a192_10270245.png "屏幕截图")
6.专题列表

![输入图片说明](https://foruda.gitee.com/images/1690252623249125750/d86b8afb_10270245.png "屏幕截图")
7.个人主页

![输入图片说明](https://foruda.gitee.com/images/1690252634586924052/e445dc0f_10270245.png "屏幕截图")
8.管理员模块

![输入图片说明](https://foruda.gitee.com/images/1690252667276544130/0982a098_10270245.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1690252674250488731/fd2d9221_10270245.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1690252682197027253/ab93fd21_10270245.png "屏幕截图")