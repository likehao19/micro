package com.lut.micro.project.controller;

import com.lut.micro.commom.response.Result;
import com.lut.micro.project.entity.Comments;
import com.lut.micro.project.service.ICommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/25 21:47
 */
@RestController
@RequestMapping("/comments")
public class CommentsController {
    @Autowired
    private ICommentsService commentsService;

    /**
     * 添加评论
     * @param comments
     * @return
     */
    @PostMapping
    public Result addComments(@RequestBody Comments comments){
        if (commentsService.addComments(comments)){
            return Result.success();
        }
        return Result.error("评论失败");
    }

    /**
     * 查询评论
     * @param articleId
     * @return
     */
    @GetMapping("{articleId}")
    public Result getComments(@PathVariable String articleId){
        return Result.success(commentsService.getComments(articleId));
    }
}
