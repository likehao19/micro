package com.lut.micro.project.controller;

import com.lut.micro.commom.response.Result;
import com.lut.micro.project.service.IUserCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/02 11:38
 */
@RestController
@RequestMapping("/userCount")
public class UserCountController {

    @Autowired
    IUserCountService userCountService;

    /**
     * 获取热门作者
     * @return
     */
    @GetMapping
    public Result getUserCount(){
        return Result.success(userCountService.getUserCount());
    }
}
