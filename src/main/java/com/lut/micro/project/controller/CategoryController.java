package com.lut.micro.project.controller;

import com.lut.micro.commom.response.Result;
import com.lut.micro.project.entity.Category;
import com.lut.micro.project.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/16 13:26
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    ICategoryService categoryService;

    /**
     * 获取分类列表
     * @return
     */
    @GetMapping
    public Result getCategoryList(){
        List<Category> lists = categoryService.selectCategoryList();
        return Result.success(lists);
    }

    /**
     * 新增分类
     */
    @PostMapping
    public Result addCategory(@RequestBody Category category){
        categoryService.insertCategory(category);
        return Result.success();
    }
}
