package com.lut.micro.project.controller;

import com.lut.micro.commom.response.Result;
import com.lut.micro.commom.utils.uuid.IdUtils;
import io.minio.*;
import io.minio.http.Method;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/09 14:21
 */
@RestController
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    private MinioClient minioClient;


    @PostMapping
    public Result uploadFile( @RequestParam("file") MultipartFile file) throws Exception {
        System.out.println(file);
        // 指定存储桶名称和根文件夹名称
        String bucketName = "mybucket";
        String rootFolder = "images";

        // 获取当前日期
        LocalDate currentDate = LocalDate.now();
        String year = String.valueOf(currentDate.getYear());
        String month = String.format("%02d", currentDate.getMonthValue());
        String day = String.format("%02d", currentDate.getDayOfMonth());

        // 构建子文件夹路径
        String folderPath = rootFolder + "/" + year + "/" + month + "/" + day + "/";

        // 指定对象名称和要上传的文件路径
        String objectName = folderPath + IdUtils.fastSimpleUUID();


        // 上传文件到子文件夹
        minioClient.putObject(
                PutObjectArgs.builder()
                        .bucket(bucketName)
                        .object(objectName)
                        .stream(file.getInputStream(), file.getSize(), -1)
                        .contentType("jpg")
                        .build()
        );
        System.out.println("File uploaded successfully to " + folderPath);
        String url=minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder().method(Method.GET).bucket(bucketName).object(objectName).build());
        return Result.success( url.substring(0, url.indexOf("?")));
    }
}
