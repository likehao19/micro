package com.lut.micro.project.controller;


import com.lut.micro.commom.response.Result;
import com.lut.micro.project.entity.Article;
import com.lut.micro.project.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/10 12:27
 */
@RestController
@RequestMapping("/article")
public class ArticleController {

    @Autowired
    IArticleService articleService;


    /**
     * 获取文章列表
     * @return
     */
    @GetMapping
    public Result getArticleList(){
        List<Article> articles =articleService.selectArticleList();
        return Result.success(articles);
    }

    /**
     * 待审核文章列表
     */
    @GetMapping("/auditList")
    public Result getAuditList(){
        return Result.success(articleService.getAuditList());
    }

    /**
     * 查询相关
     */
    @GetMapping("/relatedList/{articleId}")
    public Result getRelatedList(@PathVariable String articleId){
        return Result.success(articleService.getRelatedList(articleId));
    }
    /**
     * 获取专题列表
     * @return
     */
    @GetMapping("/topic")
    public Result getTopicList(){
        List<Article> articles =articleService.selectTopicList();
        return Result.success(articles);
    }


    @GetMapping("/getStartsList/{userId}")
    public Result getStartsList(@PathVariable String userId){
        return Result.success(articleService.getStartsList(userId));
    }

    /**
     * 根据类型获取文章列表，按热度排序
     * @param id
     * @return
     */
    @GetMapping("/category")
    public Result getArticleListById(@RequestParam("id") String id,@RequestParam("userId")String userId){
        List<Article> articles =articleService.selectArticleListById(id,userId);
        return Result.success(articles);
    }

    /**
     * 根据类型获取文章列表，按时间排序
     * @param id
     * @return
     */
    @GetMapping("/categoryByTime")
    public Result getArticleListByTime(@RequestParam("id") String id,@RequestParam("userId")String userId){
        List<Article> articles =articleService.selectArticleListByTime(id,userId);
        return Result.success(articles);
    }

    /**
     *获取热度文章
     */
    @GetMapping("/getHotList")
    public Result getHotArticles(){
        return Result.success(articleService.getHotArticles());
    }

    /**
     * 新增文章
     * @param article
     * @return
     */
     @PutMapping
     public Result insetArticle(@RequestBody Article article){
         System.out.println("article"+article);
        if (articleService.insetArticle(article)){
            return Result.success();
        }else{
            return Result.error("发布失败");
        }
     }

    /**
     * 新增草稿箱
     * @param article
     * @return
     */
    @PutMapping("/draftsArticle")
    public Result insetDraftsArticle(@RequestBody Article article){
        if (articleService.insetDraftsArticle(article)){
            return Result.success();
        }else{
            return Result.error("保存失败");
        }
    }


    /**
     * 根据文章id获取文章详情
     * @param articleId
     * @return
     */
     @GetMapping("{articleId}")
     public Result getArticleById(@PathVariable String articleId){
           Article article=articleService.selectArticleById(articleId);
           return Result.success(article);
     }
    /**
     * 用户发起审核
     */
    @GetMapping("/startReview/{articleId}")
    public Result startReviewArticle(@PathVariable String articleId){
       if (articleService.startReviewArticle(articleId)){
           return Result.success();
       }
        return Result.error("发起审核失败");
    }

    /**
     * 审核文章
     */
    @GetMapping("/review")
     public Result reviewArticle(@RequestParam("articleId") String articleId,@RequestParam("isReview") String isReview){
        if (articleService.reviewArticle(articleId,isReview)){
            return Result.success();
        }else{
            return Result.error("审核不通过");
        }
     }
    /**
     * 撤回审核
     */
    @GetMapping("/cancelReview/{articleId}")
    public Result cancelReview(@PathVariable String articleId){
        if (articleService.cancelReview(articleId)){
            return Result.success("撤销成功");
        }else{
            return Result.error("不允许撤销");
        }
    }
    /**
     * 查询我的文章
     */
    @GetMapping("/list/{userId}")
    public Result getArticle(@PathVariable String userId){
        List<Article> articles = articleService.getArticle(userId);
        return Result.success(articles);
    }

    /**
     * 草稿箱
     */
    @GetMapping("/drafts/{userId}")
    public Result getDrafts(@PathVariable String userId){
        return Result.success(articleService.getDrafts(userId));
    }
    /**
     * 待审核文章
     */
    @GetMapping("/audit/{userId}")
    public Result getAudit(@PathVariable String userId){
        return Result.success(articleService.getAudit(userId));
    }
    /**
     * 审核中文章
     */
    @GetMapping("/reviewing/{userId}")
    public Result getReviewing(@PathVariable String userId){
        return Result.success(articleService.getReviewing(userId));
    }

    /**
     * 回收站
     */
    @GetMapping("/recycle/{userId}")
    public Result getRecycle(@PathVariable String userId){
        return Result.success(articleService.getRecycle(userId));
    }

    /**
     * 删除文章
     */
    @DeleteMapping("{articleId}")
    public Result deleteArticle(@PathVariable String articleId){
        if (articleService.deleteArticle(articleId)){
            return Result.success();
        }
        return Result.error("删除失败");
    }

    /**
     * 删除回收站
     */
    @DeleteMapping("/delArticle")
    public Result delArticle(@RequestParam("articleId") String articleId){
        articleService.realDelArticle(articleId);
        return Result.success();
    }

    /**
     * 文章检索
     */
    @GetMapping("/fuzzySearch/{keyword}")
    public Result fuzzySearch(@PathVariable String keyword){
        List<Article> articleList=articleService.fuzzySearch(keyword);
        if (articleList.isEmpty()){
            return Result.error("检索无相关内容！");
        }
        return Result.success(articleList);
    }
}
