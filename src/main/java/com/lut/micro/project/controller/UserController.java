package com.lut.micro.project.controller;

import com.lut.micro.project.entity.UserSetting;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.lut.micro.commom.response.Result;
import com.lut.micro.commom.utils.ip.IpUtils;
import com.lut.micro.project.entity.LoginBody;
import com.lut.micro.project.entity.User;
import com.lut.micro.project.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/04/25 13:58
 */
@RestController
@RequestMapping("/user")
public class UserController {
    private static final Log logger = LogFactory.getLog(UserController.class);

      @Autowired
      private IUserService userService;

    /**
     * 用户注册
     * @param user
     * @return
     */
    @PostMapping("/register")
    public Result register(@RequestBody User user){
        System.out.println(user);
        if (!userService.checkCode(user)){
            return Result.error("验证码已失效");
        } else if (userService.checkUserNameUnique(user)>0){
            return Result.error("注册失败,"+user.getUserName()+"账号已存在");
        } else if (userService.checkEmailUnique(user)>0) {
            return Result.error("注册失败,"+user.getEmail()+"邮箱已存在");
        } else{
            userService.register(user);
            return Result.success();
        }
    }

    /**
     * 登录
     * @param loginBody
     * @param request
     * @return
     */
    @PostMapping("/login")
    public Result login(@RequestBody LoginBody loginBody,HttpServletRequest request){
        String token= userService.login(loginBody,request);
        if (token==null){
            return Result.error("用户名或密码错误");
        }
        if (token.length()<20){
            return Result.error("该账号已被封禁!");
        }
        logger.info("登录成功！"+"Token令牌："+token);
        return Result.success(token);
    }

    /**
     * 邮箱验证码登录
     * @param loginBody
     * @return
     */
    @PostMapping("/loginWithEmail")
    public Result loginWithEmail(@RequestBody LoginBody loginBody){
        String token= userService.loginWithEmail(loginBody);
        if (token==null){
            return Result.error("邮箱或验证码错误");
        }
        return Result.success(token);
    }

    /**
     * 根据token获取用户信息
     * @param request
     * @return
     */
    @GetMapping("/getUserInfo")
    public Result getUserInfo(HttpServletRequest request){
        String token = request.getHeader("Authorization");
        if (token == null || token.isEmpty()) {
            // 处理 token 不存在的情况
            return Result.error("请求错误");
        }else{
            User userInfo=userService.getUserInfo(token);
            return Result.success(userInfo);
        }
    }

    /**
     * 根据userId获取用户信息
     * @return
     */
    @GetMapping("/getUserInfoById/{userId}")
    public Result getUserInfoById(@PathVariable String userId){
       return Result.success(userService.getUserInfoById(userId));
    }


    /**
     * 获取用户列表
     * @return
     */
    @GetMapping
    public Result getUserList(){
        return  Result.success(userService.getUserList());
    }


    /**
     * 封禁用户列表
     */
    @GetMapping("/banList")
    public Result getBanList(){
        return Result.success(userService.getBanList());
    }

    /**
     * 更新用户信息
     * @param user
     * @param request
     * @return
     */
    @PostMapping
    public Result updateUserInfo(@RequestBody User user,HttpServletRequest request){
        if (userService.checkEmailUnique(user)>1){
            return Result.error("邮箱已存在");
        }else if (userService.checkPhoneUnique(user)>1){
            return Result.error("手机号已存在");
        }
        userService.updateUserInfo(user);
        return Result.success();
    }

    /**
     * 修改密码
     * @param user
     * @return
     */
    @PostMapping("/updatePwd")
    public Result updatePassword(@RequestBody User user){
        userService.updatePassword(user);
        return Result.success();
    }

    /**
     * 检验验证码
     * @param user
     * @return
     */
    @PostMapping("/checkCode")
    public Result checkCode(@RequestBody User user){
        if (!userService.checkCode(user)){
            return Result.error("验证码已失效");
        }
        return Result.success();
    }

    //关注
    @PostMapping("/followUser")
    public Result followUser(@RequestParam String userFrom,@RequestParam String userTo){
        logger.info("userFrom"+userFrom);
        logger.info("userTo"+userTo);
        userService.followUser(userFrom,userTo);
        return Result.success();
    }
    //取消关注
    @PostMapping("/canelFollow")
    public Result canelFollow(@RequestParam String userFrom,@RequestParam String userTo){
        userService.canelFollow(userFrom,userTo);
        return Result.success();
    }

    //查询用户是否被关注
    @PostMapping ("/getFollow")
    public Result getFollow(@RequestParam String userFrom,@RequestParam String userTo){
        if (userService.getFollow(userFrom,userTo)){
            return Result.success();
        }
        return Result.error("用户未被关注");
    }

    //查询设置
    @GetMapping("/getSetting/{userId}")
    public Result getUserSetting(@PathVariable String userId){
        return Result.success(userService.getUserSetting(userId));
    }

    //更改设置
    @PostMapping ("/updateSetting")
    public Result updateSetting(@RequestBody UserSetting userSetting){
        userService.updateUserSetting(userSetting);
        return Result.success();
    }

    //获取我的关注列表
    @GetMapping("/getFollowList/{userId}")
    public Result getFollowList(@PathVariable String userId){
        return Result.success(userService.getFollowList(userId));
    }

    //获取关注我的列表
    @GetMapping("/getFollowedList/{userId}")
    public Result getFollowedList(@PathVariable String userId){
        return Result.success(userService.getFollowedList(userId));
    }

    //封禁或解封
    @PostMapping("/updateStatus")
    public Result updateStatus(@RequestParam("userId") String userId,@RequestParam("status") String status){
        System.out.println(userId+status);
        if (userService.updateStatus(userId,status)){
            return Result.success();
        }
        return Result.error();
    }

}
