package com.lut.micro.project.controller;

import cn.hutool.extra.spring.SpringUtil;
import com.lut.micro.commom.redis.RedisUtils;
import com.lut.micro.commom.response.CommonResult;
import com.lut.micro.commom.response.Result;
import com.lut.micro.commom.utils.RandomCodeUtils;
import com.lut.micro.project.entity.MailRequest;
import com.lut.micro.project.service.SendMailService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/06 15:56
 */
@RestController
@RequestMapping("/send-mail")
@Api(value = "发送邮件接口",tags = {"发送邮件接口"})
public class SendMailController {
    @Autowired
    private SendMailService sendMailService;

    /**
     * 发送简单邮箱
     * @param email
     */
  @GetMapping("/simple{email}")
  public void SendSimpleMessage(@PathVariable String email) {
      System.out.println(email);
      MailRequest mailRequest=new MailRequest();
      RandomCodeUtils randomCodeUtils=new RandomCodeUtils();
      String code = randomCodeUtils.getRandomCodeWithNum();
      mailRequest.setSendTo(email);
      mailRequest.setSubject("验证码");
      mailRequest.setText(
              "您的验证码是:"+"<h1>"+code+"</h1>"+"(30分钟内有效)"
      );
      RedisUtils redisUtils = SpringUtil.getBean(RedisUtils.class);
      redisUtils.set(email,code,1800);
      sendMailService.sendSimpleMail(mailRequest);
  }

    /**
     * 发送简单邮箱
     */
    @PostMapping("/html")
    public void SendHtmlMessage(@RequestBody MailRequest mailRequest) {
        sendMailService.sendHtmlMail(mailRequest);
    }

}
