package com.lut.micro.project.controller;

import com.lut.micro.commom.response.Result;
import com.lut.micro.project.entity.Message;
import com.lut.micro.project.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/10 15:26
 */
@RestController
@RequestMapping("/message")
public class MessageController {
    @Autowired
    IMessageService messageService;

    //查询聊天记录
    @PostMapping("/getMessage")
    public Result getMessage(@RequestParam String userFrom,@RequestParam String userTo){
           return Result.success(messageService.getMessage(userFrom,userTo));
    }

    //查询私信列表
    @GetMapping("{userId}")
    public Result getMessageToMine(@PathVariable String userId){
        return Result.success(messageService.getMessageToMine(userId));
    }

    //发送消息
    @PostMapping("/addMessage")
    public Result addMessage(@RequestBody Message message){
        if ("0".equals(messageService.addMessage(message))){
            return Result.success();
        }
        return Result.error("发送失败,对方已设置私信权限");
    }
}
