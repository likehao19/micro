package com.lut.micro.project.controller;

import com.lut.micro.commom.response.Result;
import com.lut.micro.project.service.IArticleCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/30 18:12
 */
@RestController
@RequestMapping("/articleCount")
public class ArticleCountController {

    @Autowired
    private IArticleCountService articleCountService;


    /**
     * 浏览量
     * @param articleId
     * @return
     */
    @PutMapping("/views/{articleId}")
    public Result updateViews(@PathVariable String articleId){
        articleCountService.updateViews(articleId);
        return Result.success();
    }

    /**
     * 点赞量
     * @param articleId
     * @param userId
     * @param oper
     * @return
     */
    @PutMapping("/starts/{articleId}/{userId}/{oper}")
    public Result updateStarts(@PathVariable("articleId") String articleId, @PathVariable("userId") String userId,@PathVariable("oper") String oper) {
            articleCountService.updateStarts(articleId, userId,oper);
        return Result.success();
    }

    /**
     * 检查用户是否点赞
     * @param articleId
     * @param userId
     * @return
     */
    @PutMapping("/checkStarts/{articleId}/{userId}")
    public Result checkUserStarts(@PathVariable("articleId") String articleId, @PathVariable("userId") String userId){
        if (!articleCountService.checkUserStarts(articleId,userId)){
            return Result.success();
        }else {
            return Result.error();
        }
    }


    /**
     * 获取文章统计量
     * @param articleId
     * @return
     */
    @GetMapping("/{articleId}")
    public Result getArticleCount(@PathVariable String articleId){
        return Result.success(articleCountService.getArticleCount(articleId));
    }

}
