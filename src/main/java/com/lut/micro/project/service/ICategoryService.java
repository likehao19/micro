package com.lut.micro.project.service;

import com.lut.micro.project.entity.Category;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/16 13:27
 */
public interface ICategoryService {

    List<Category> selectCategoryList();

    boolean insertCategory(Category category);
}
