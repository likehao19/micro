package com.lut.micro.project.service;

import com.lut.micro.project.entity.Article;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/12 14:50
 */
public interface IArticleService {
    boolean insetArticle(Article article);

    List<Article> selectArticleList();

    List<Article> selectArticleListById(String id,String userId);

    Article selectArticleById(String articleId);

    List<Article> selectTopicList();

    boolean reviewArticle(String articleId,String isReview);

    List<Article> getArticle(String userId);

    List<Article>  getDrafts(String userId);

    List<Article> getAudit(String userId);

    List<Article> getReviewing(String userId);

    List<Article> getRecycle(String userId);

    boolean insetDraftsArticle(Article article);

    boolean deleteArticle(String articleId);

    List<Article> getAuditList();

    List<Article> getRelatedList(String articleId);

    boolean startReviewArticle(String articleId);

    boolean cancelReview(String articleId);

    List<Article> selectArticleListByTime(String id,String userId);

    List<Article> getStartsList(String userId);

    List<Article> getHotArticles();

    List<Article> fuzzySearch(String keyword);

    boolean realDelArticle(String articleId);
}
