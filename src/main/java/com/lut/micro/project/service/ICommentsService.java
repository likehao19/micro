package com.lut.micro.project.service;

import com.lut.micro.project.entity.Comments;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/25 21:48
 */

public interface ICommentsService {
    boolean addComments(Comments comments);

    List<Comments> getComments(String articleId);
}
