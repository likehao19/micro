package com.lut.micro.project.service;

import com.lut.micro.commom.response.Result;
import com.lut.micro.project.entity.Follow;
import com.lut.micro.project.entity.LoginBody;
import com.lut.micro.project.entity.User;
import com.lut.micro.project.entity.UserSetting;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/04/25 14:00
 */
public interface IUserService {

   void register(User user);

   int checkUserNameUnique(User user);

   int checkEmailUnique(User user);

   boolean checkCode(User user);

   String login(LoginBody loginBody, HttpServletRequest request);

   User getUserInfo(String token);

   void updateUserInfo(User user);

   int checkPhoneUnique(User user);

   boolean updatePassword(User user);

   String loginWithEmail(LoginBody loginBody);

   List<User> getUserList();

   List<User> getBanList();

   boolean followUser(String userFrom, String userTo);

   boolean canelFollow(String userFrom, String userTo);

   boolean getFollow(String userFrom, String userTo);

   User getUserInfoById(String userId);

   boolean updateUserSetting(UserSetting userSetting);

   UserSetting getUserSetting(String userId);

   List<User> getFollowList(String userId);

   List<User> getFollowedList(String userId);

    boolean updateStatus(String userId, String status);
}
