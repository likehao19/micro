package com.lut.micro.project.service;

import com.lut.micro.project.entity.Message;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/10 15:31
 */

public interface IMessageService {
    List<Message> getMessage(String userFrom, String userTo);

    List<Message> getMessageToMine(String userId);

    String addMessage(Message message);
}
