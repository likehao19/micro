package com.lut.micro.project.service;

import com.lut.micro.project.entity.User;
import com.lut.micro.project.entity.UserCount;
import com.lut.micro.project.entity.dto.UserCountDTO;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/02 11:39
 */
public interface IUserCountService {

    List<UserCountDTO> getUserCount();
}
