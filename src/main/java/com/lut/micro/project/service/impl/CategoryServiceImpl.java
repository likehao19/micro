package com.lut.micro.project.service.impl;

import com.lut.micro.commom.response.Result;
import com.lut.micro.project.dao.CategoryMapper;
import com.lut.micro.project.entity.Category;
import com.lut.micro.project.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/16 13:27
 */
@Service
public class CategoryServiceImpl implements ICategoryService {

    @Autowired
    CategoryMapper categoryMapper;


    @Override
    public List<Category> selectCategoryList() {
        return categoryMapper.selectCategoryList();
    }

    @Override
    public boolean insertCategory(Category category) {
        return categoryMapper.insertCategory(category);
    }
}
