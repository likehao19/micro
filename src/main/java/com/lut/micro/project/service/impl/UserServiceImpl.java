package com.lut.micro.project.service.impl;

import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lut.micro.commom.redis.RedisUtils;
import com.lut.micro.commom.security.interceptor.TokenInterceptor;
import com.lut.micro.commom.utils.ip.IpUtils;
import com.lut.micro.commom.utils.uuid.IdUtils;
import com.lut.micro.project.dao.FollowMapper;
import com.lut.micro.project.dao.UserCountMapper;
import com.lut.micro.project.dao.UserMapper;
import com.lut.micro.project.dao.UserSettingMapper;
import com.lut.micro.project.entity.Follow;
import com.lut.micro.project.entity.LoginBody;
import com.lut.micro.project.entity.User;
import com.lut.micro.project.entity.UserSetting;
import com.lut.micro.project.service.IUserService;
import lombok.val;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.lut.micro.commom.utils.BcryptUtils.hashPassword;
import static com.lut.micro.commom.utils.BcryptUtils.verifyPassword;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/04/25 14:01
 */
@Service
public class UserServiceImpl implements IUserService {

    private static final Log logger = LogFactory.getLog(UserServiceImpl.class);

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserCountMapper userCountMapper;

    @Autowired
    private FollowMapper followMapper;

    @Autowired
    private UserSettingMapper userSettingMapper;

    RedisUtils redisUtils = SpringUtil.getBean(RedisUtils.class);

    @Override
    public void register(User user) {
        user.setUserId(IdUtils.fastSimpleUUID());
        user.setPassword(hashPassword(user.getPassword()));
        userMapper.insertUser(user);
        val userId = user.getUserId();
        userSettingMapper.insertUserSetting(userId);
        userMapper.insertUserCount(user);
    }

    @Override
    public int checkUserNameUnique(User user) {
        int res=userMapper.selectUserNameUnique(user);
        return res;
    }

    @Override
    public int checkEmailUnique(User user) {
        int res=userMapper.selectEmailUnique(user);
        return res;
    }

    @Override
    public boolean checkCode(User user) {
        return redisUtils.hasKey(user.getEmail()) && redisUtils.get(user.getEmail()).equals(user.getCode());
    }

    @Override
    public String login(LoginBody loginBody, HttpServletRequest request) {
        String token=IdUtils.fastSimpleUUID();
        User user=new User()
                .setUserName(loginBody.getUserName())
                .setPassword(loginBody.getPassword());
        user.setUserName(loginBody.getUserName());
        user.setPassword(loginBody.getPassword());
        User u=userMapper.selectUser(user);
        //用户是否被封禁
        if ("1".equals(u.getStatus())){
            return "该账号已被封禁!";
        }
        if (u!=null){
            if (verifyPassword(user.getPassword(), u.getPassword())){
                u.setLoginIp(IpUtils.getIpAddress(request));
                userMapper.updateLoginMsg(u);
                redisUtils.set("token:"+token,u.getUserId(),1800);
                logger.info("token:"+token+"设置30分钟");
                redisUtils.set("userId:"+u.getUserId(), JSON.toJSONString(u),1800);
                logger.info("userId:"+u.getUserId()+"设置30分钟");
            }else{
                return null;
            }
        }

        return token;
    }

    @Override
    public String loginWithEmail(LoginBody loginBody) {
        String token=IdUtils.fastSimpleUUID();
        User user=new User()
                .setEmail(loginBody.getEmail())
                .setCode(loginBody.getCode());
        User u=userMapper.selectUserWithEmail(user);
        if (u!=null){
            if (redisUtils.hasKey(user.getEmail()) && redisUtils.get(user.getEmail()).equals(user.getCode())){
                redisUtils.set("token:"+token,u.getUserId(),1800);
                redisUtils.set("userId:"+u.getUserId(), JSON.toJSONString(u));
            }else{
                return null;
            }
        }
        return token;
    }

    @Override
    public List<User> getUserList() {
        return userMapper.selectUserList();
    }

    @Override
    public List<User> getBanList() {
        return userMapper.getBanList();
    }

    @Override
    public boolean followUser(String userFrom, String userTo) {
        userCountMapper.updateAddFollow(userFrom);
        userCountMapper.updateAddFans(userTo);
        followMapper.insertFollow(userFrom,userTo);
        return redisUtils.hset(userTo, userFrom, String.valueOf(System.currentTimeMillis()));
    }

    @Override
    public boolean canelFollow(String userFrom, String userTo) {
        userCountMapper.updateSubFollow(userFrom);
        userCountMapper.updateSubFans(userTo);
        followMapper.deleteFollow(userFrom,userTo);
        redisUtils.hdel(userTo,userFrom);
        return true;
    }

    @Override
    public boolean getFollow(String userFrom, String userTo) {
        if (redisUtils.hmget(userTo).isEmpty()){
            return false;
        }
        return redisUtils.hmget(userTo).keySet().stream().anyMatch(key->key.equals(userFrom));
    }

    @Override
    public User getUserInfoById(String userId) {
        return userMapper.getUserInfoById(userId);
    }

    @Override
    public boolean updateUserSetting(UserSetting userSetting) {
        userSettingMapper.updateUserSetting(userSetting);
        return true;
    }

    @Override
    public UserSetting getUserSetting(String userId) {
        return userSettingMapper.getUserSetting(userId);
    }
    //我的关注
    @Override
    public List<User> getFollowList(String userId) {
        List<Follow> followList = followMapper.getFollowList(userId);
        return followList.stream()
                .map(follow -> {
                    User user = userMapper.selectUserById(follow.getFollowed());
                    return new User()
                            .setUserId(user.getUserId())
                            .setNickName(user.getNickName())
                            .setAvatar(user.getAvatar())
                            .setNote(user.getNote())
                            .setLevel(user.getLevel());
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<User> getFollowedList(String userId) {
        List<Follow> followedList = followMapper.getFollowedList(userId);
        return followedList.stream()
                .map(follow -> {
                    User user = userMapper.selectUserById(follow.getFollow());
                    return new User()
                            .setUserId(user.getUserId())
                            .setNickName(user.getNickName())
                            .setAvatar(user.getAvatar())
                            .setNote(user.getNote())
                            .setLevel(user.getLevel());
                })
                .collect(Collectors.toList());
    }

    @Override
    public boolean updateStatus(String userId, String status) {
        //封禁
        if ("1".equals(status)){
            userMapper.updateBanStatus(userId,status);
            return true;
        }
        //解封
        if ("0".equals(status)){
            userMapper.updateStatus(userId,status);
            return true;
        }
        return false;
    }


    /**
     * 根据token获取用户信息
     * @param token
     * @return
     */
    @Override
    public User getUserInfo(String token) {
        String userId=redisUtils.get("token:"+token).toString();
        String context =redisUtils.get("userId:"+userId).toString();
        try{
            User user = JSONObject.parseObject(context, User.class);
            User u=userMapper.selectUser(user);
            return u;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateUserInfo(User user) {
         userMapper.updateUserInfo(user);
    }

    @Override
    public int checkPhoneUnique(User user) {
        return userMapper.selectPhoneUnique(user);
    }

    @Override
    public boolean updatePassword(User user) {
        User u=new User();
        u.setUserId(user.getUserId());
        u.setPassword(hashPassword(user.getPassword()));
        int res=userMapper.updatePassword(u);
        return res>=0;
    }


}
