package com.lut.micro.project.service.impl;

import com.github.houbb.sensitive.word.core.SensitiveWordHelper;
import com.lut.micro.commom.response.Result;
import com.lut.micro.commom.utils.SendSimpleMessageUtils;
import com.lut.micro.project.controller.UserController;
import com.lut.micro.project.dao.*;
import com.lut.micro.project.entity.Article;
import com.lut.micro.project.entity.Follow;
import com.lut.micro.project.entity.MailRequest;
import com.lut.micro.project.entity.User;
import com.lut.micro.project.service.IArticleService;
import com.lut.micro.project.service.SendMailService;
import io.swagger.models.auth.In;
import lombok.val;
import org.ansj.recognition.impl.StopRecognition;
import org.ansj.splitWord.analysis.ToAnalysis;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/12 14:50
 */
@Service
public class ArticleServiceImpl implements IArticleService {

    private static final Log logger = LogFactory.getLog(ArticleServiceImpl.class);

    @Autowired
    ArticleMapper articleMapper;

    @Autowired
    CategoryMapper categoryMapper;

    @Autowired
    ArticleCountMapper articleCountMapper;

    @Autowired
    UserCountMapper userCountMapper;

    @Autowired
    StartsMapper startsMapper;

    @Autowired
    FollowMapper followMapper;

    @Autowired
    private SendMailService sendMailService;

    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean insetArticle(Article article) {
        if (article.getArticleId() == null){
            articleMapper.insetArticle(article);
            val articleId = article.getArticleId();
            logger.info("articleRes"+articleId);
            //初始化文章统计表
            articleCountMapper.insertArticleCount(articleId);
            //初始化用户统计
            // userCountMapper.updateArticleAddCount(article.getUserId());
            // logger.info("用户文章数量加1");
            //categoryMapper.updateCount(article.getCategoryId());
            logger.info("分类数量加1");
        }else {
            articleMapper.updateArticle(article.getArticleId());
        }
        return true;
    }

    @Override
    public List<Article> selectArticleList() {
        return articleMapper.selectArticleList();
    }

    @Override
    public List<Article> selectArticleListById(String id,String userId) {
        if ("98".equals(id)){
            List<Follow> followList = followMapper.getFollowList(userId);
            List<Article> list = followList.parallelStream()
                    .map(follow -> articleMapper.getArticle(follow.getFollowed()))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
            return list;
        }
        return articleMapper.selectArticleListById(id);
    }
    @Override
    public List<Article> selectArticleListByTime(String id,String userId) {
        if ("98".equals(id)){
            List<Follow> followList = followMapper.getFollowList(userId);
            List<Article> list = followList.parallelStream()
                    .map(follow -> articleMapper.getArticle(follow.getFollowed()))
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
            return list;
        }
        return articleMapper.selectArticleListByTime(id);
    }
    @Override
    public Article selectArticleById(String articleId) {
        return articleMapper.selectArticleById(articleId);
    }

    @Override
    public List<Article> selectTopicList() {
        return articleMapper.selectTopicList();
    }

    @Override
    public boolean reviewArticle(String articleId,String isReview) {
        if ("yes".equals(isReview)) {
            Article article = articleMapper.selectArticleById(articleId);
            User user=userMapper.selectUserById(article.getUserId());
            //分词
            StopRecognition stopRecognition = new StopRecognition();
            stopRecognition.insertStopRegexes("[a-zA-Z0-9!@#$%^&*()=_+\\?、？\\-！~`.,;:'\\\"<>?/\\|\\]\\[{}]+$");
            String analysedText = ToAnalysis.parse(article.getMarkdown())
                    .recognition(stopRecognition)
                    .toStringWithOutNature()
                    .replaceAll(",", " ");
            List<String> distinctWords = Arrays.stream(analysedText.split(" "))
                    .parallel()
                    .distinct()
                    .collect(Collectors.toList());
            System.out.println("文章是否包含敏感词:");
            boolean containsSensitiveWord = SensitiveWordHelper.contains(analysedText);
            System.out.println(containsSensitiveWord);

            //包含敏感字
            if (containsSensitiveWord) {
                articleMapper.cancelReview(articleId);
                MailRequest res = SendSimpleMessageUtils.SendSimpleMessage(user.getEmail(), article.getTitle(), "审核不通过");
                sendMailService.sendSimpleMail(res);
                return false;
            } else {
                articleMapper.reviewRes(articleId);
                categoryMapper.updateCategoryAddCount(article.getCategoryId());
                userCountMapper.updateArticleAddCount(article.getUserId());
                MailRequest res = SendSimpleMessageUtils.SendSimpleMessage(user.getEmail(), article.getTitle(), "审核已通过");
                sendMailService.sendSimpleMail(res);
                return true;
            }
        }
        else if ("no".equals(isReview)){
            articleMapper.cancelReview(articleId);
            return false;
        }
        return false;
    }

    @Override
    public List<Article> getArticle(String userId) {
        return articleMapper.getArticle(userId);
    }

    @Override
    public List<Article> getDrafts(String userId) {
        return articleMapper.getDrafts(userId);
    }

    @Override
    public List<Article> getAudit(String userId) {
        return articleMapper.getAudit(userId);
    }

    @Override
    public List<Article> getReviewing(String userId) {
        return articleMapper.getReviewing(userId);
    }

    @Override
    public List<Article> getRecycle(String userId) {
        return articleMapper.getRecycle(userId);
    }

    @Override
    public boolean insetDraftsArticle(Article article) {
        System.out.println(article);
        if (article.getArticleId()==null){
            articleMapper.insetDraftsArticle(article);
        }else {
            articleMapper.updateDraftsArticle(article);
        }
        return true;
    }

    @Override
    public boolean deleteArticle(String articleId) {
        Article article=articleMapper.selectArticleById(articleId);
        boolean res=articleMapper.deleteArticle(article);
        if ("$shtg".equals(article.getStatus())){
            //分类数减一
            categoryMapper.updateCategorySubCount(article.getCategoryId());
            //用户文章数减一
            userCountMapper.updateArticleSubCount(article.getUserId());
            //文章统计表数据清空
            articleCountMapper.deleteArticle(article.getArticleId());
        }
        return res;
    }

    @Override
    public List<Article> getAuditList() {
        return articleMapper.getAuditList();
    }

    @Override
    public List<Article> getRelatedList(String articleId) {
        return articleMapper.getRelatedList(articleId);
    }

    @Override
    public boolean startReviewArticle(String articleId) {
        return articleMapper.startReviewArticle(articleId);
    }

    @Override
    public boolean cancelReview(String articleId) {
        return articleMapper.cancelReview(articleId);
    }



    @Override
    public List<Article> getStartsList(String userId) {
        List<Article> articleIdList=startsMapper.getStartsList(userId);
        List<Article> articleList = articleIdList.stream()
                .map(a -> articleMapper.selectArticleById(a.getArticleId().toString()))
                .collect(Collectors.toList());
        return articleList;
    }

    @Override
    public List<Article> getHotArticles() {
        return articleMapper.getHotArticles();
    }

    @Override
    public List<Article> fuzzySearch(String keyword) {
        return articleMapper.fuzzySearch(keyword);
    }

    @Override
    public boolean realDelArticle(String articleId) {
        articleMapper.realDelArticle(articleId);
        return true;
    }
}
