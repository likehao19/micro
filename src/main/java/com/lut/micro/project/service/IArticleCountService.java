package com.lut.micro.project.service;

import com.lut.micro.project.entity.ArticleCount;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/30 18:12
 */
public interface IArticleCountService {
    void updateViews(String articleId);

    void updateStarts(String articleId,String userId,String oper);

    ArticleCount getArticleCount(String articleId);

    boolean checkUserStarts(String articleId,String userId);
}
