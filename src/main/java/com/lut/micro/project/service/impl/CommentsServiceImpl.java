package com.lut.micro.project.service.impl;

import com.github.houbb.sensitive.word.core.SensitiveWordHelper;
import com.lut.micro.project.dao.CommentsMapper;
import com.lut.micro.project.dao.UserMapper;
import com.lut.micro.project.entity.Comments;
import com.lut.micro.project.entity.User;
import com.lut.micro.project.entity.dto.CUserDTO;
import com.lut.micro.project.service.ICommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/25 21:48
 */
@Service
public class CommentsServiceImpl implements ICommentsService {

    @Autowired
    private CommentsMapper commentsMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean addComments(Comments comments) {
        System.out.println("文本："+comments.getContent());
        System.out.println("检验结果："+SensitiveWordHelper.contains(comments.getContent()));
        if (!SensitiveWordHelper.contains(comments.getContent())){
            commentsMapper.addComments(comments);
            return true;
        }
        return false;
    }

    @Override
    public List<Comments> getComments(String articleId) {
        List<Comments> comments = commentsMapper.getComments(articleId);
        CUserDTO u=new CUserDTO();
        for (Comments comment : comments) {
            User user=userMapper.selectUserById(comment.getUserId());
            u.setUsername(user.getNickName());
            u.setAvatar(user.getAvatar());
            u.setLevel(user.getLevel());
            u.setHomeLink(user.getUserId());
            comment.setUser(u);
        }
        return comments;
    }
}
