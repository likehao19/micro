package com.lut.micro.project.service.impl;

import com.lut.micro.project.dao.MessageMapper;
import com.lut.micro.project.dao.UserSettingMapper;
import com.lut.micro.project.entity.Message;
import com.lut.micro.project.entity.UserSetting;
import com.lut.micro.project.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/10 15:32
 */
@Service
public class MessageServiceImpl implements IMessageService {
    @Autowired
    MessageMapper messageMapper;

    @Autowired
    UserSettingMapper userSettingMapper;

    @Override
    public List<Message> getMessage(String userFrom, String userTo) {
        List<Message> messages=messageMapper.getMessage(userFrom,userTo);
        for (Message message : messages) {
            if (message.getUserFromId().equals(userFrom)){
                message.setIsI(true);
            }else {
                message.setIsI(false);
            }
        }
        return messages;
    }

    @Override
    public List<Message> getMessageToMine(String userId) {
        List<Message> messages = messageMapper.getMessageToMine(userId);
        return messages;
    }

    @Override
    public String addMessage(Message message) {
        messageMapper.addMessage(message);
        UserSetting userSetting = userSettingMapper.getUserSetting(message.getUserToId());
        return userSetting.getMessageSetting();
    }
}
