package com.lut.micro.project.service.impl;

import cn.hutool.extra.spring.SpringUtil;
import com.lut.micro.commom.redis.RedisUtils;
import com.lut.micro.project.dao.ArticleCountMapper;
import com.lut.micro.project.dao.StartsMapper;
import com.lut.micro.project.entity.ArticleCount;
import com.lut.micro.project.service.IArticleCountService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/30 18:13
 */
@Service
public class ArticleCountServiceImpl implements IArticleCountService {

    private static final Log logger = LogFactory.getLog(ArticleCountServiceImpl.class);

    @Autowired
    ArticleCountMapper articleCountMapper;

    @Autowired
    StartsMapper startsMapper;

    RedisUtils redisUtils = SpringUtil.getBean(RedisUtils.class);

    @Override
    public void updateViews(String articleId) {
        articleCountMapper.updateViews(articleId);
    }

    @Override
    public void updateStarts(String articleId,String userId,String oper) {
        logger.info("articleId"+articleId);
        if ("add".equals(oper)){
            redisUtils.hset(articleId,userId,String.valueOf(System.currentTimeMillis()));
            startsMapper.insertStarts(articleId,userId);
            articleCountMapper.updateStartsAdd(articleId);
        }
        else if("sub".equals(oper)){
            redisUtils.hdel(articleId,userId);
            articleCountMapper.updateStartsSub(articleId);
            startsMapper.deleteStarts(articleId,userId);
        }


    }

    @Override
    public ArticleCount getArticleCount(String articleId) {
        return articleCountMapper.getArticleCount(articleId);
    }

    @Override
    public boolean checkUserStarts(String articleId,String userId) {
        logger.info("keys："+redisUtils.hmget(articleId));
        if (redisUtils.hmget(articleId).isEmpty()){
            return false;
        }
        return !redisUtils.hmget(articleId).keySet().stream().anyMatch(key->key.equals(userId));
    }
}
