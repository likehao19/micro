package com.lut.micro.project.service.impl;

import com.lut.micro.project.dao.UserCountMapper;
import com.lut.micro.project.dao.UserMapper;
import com.lut.micro.project.entity.User;
import com.lut.micro.project.entity.dto.UserCountDTO;
import com.lut.micro.project.service.IUserCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/02 11:39
 */
@Service
public class UserCountServiceImpl implements IUserCountService {

    @Autowired
    public UserCountMapper userCountMapper;

    @Autowired
    public UserMapper userMapper;

    @Override
    public List<UserCountDTO> getUserCount() {
        return userCountMapper.getUserCountList();
    }
}
