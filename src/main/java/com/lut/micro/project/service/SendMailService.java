package com.lut.micro.project.service;

import com.lut.micro.project.entity.MailRequest;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/06 15:54
 */
public interface SendMailService {

    /**
     * 简单文本邮件
     *
     * @param mailRequest
     * @return
     */
    void sendSimpleMail(MailRequest mailRequest);


    /**
     * Html格式邮件,可带附件
     *
     * @param mailRequest
     * @return
     */
    void sendHtmlMail(MailRequest mailRequest);

}
