package com.lut.micro.project.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (UserSetting)实体类
 *
 * @author makejava
 * @since 2023-06-10 21:52:51
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UserSetting implements Serializable {
    private static final long serialVersionUID = 404209817243415518L;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 私信设置(0所有人，1我关注的人，2互相关注的人，4关闭)
     */
    private String messageSetting;
    /**
     * 消息推送(0推送，1不推送)
     */
    private String systemSetting;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessageSetting() {
        return messageSetting;
    }

    public void setMessageSetting(String messageSetting) {
        this.messageSetting = messageSetting;
    }

    public String getSystemSetting() {
        return systemSetting;
    }

    public void setSystemSetting(String systemSetting) {
        this.systemSetting = systemSetting;
    }

}

