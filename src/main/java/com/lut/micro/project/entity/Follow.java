package com.lut.micro.project.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Follow)实体类
 *
 * @author makejava
 * @since 2023-06-10 21:25:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Follow implements Serializable {
    private static final long serialVersionUID = 722078932646887320L;

    private Integer id;
    /**
     * 关注者
     */
    private String follow;
    /**
     * 被关注者
     */
    private String followed;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFollow() {
        return follow;
    }

    public void setFollow(String follow) {
        this.follow = follow;
    }

    public String getFollowed() {
        return followed;
    }

    public void setFollowed(String followed) {
        this.followed = followed;
    }

}

