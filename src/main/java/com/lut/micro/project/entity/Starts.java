package com.lut.micro.project.entity;

import java.io.Serializable;

/**
 * (Starts)实体类
 *
 * @author makejava
 * @since 2023-06-10 23:21:45
 */
public class Starts implements Serializable {
    private static final long serialVersionUID = -48476043970777540L;

    private Integer id;
    /**
     * 文章ID
     */
    private String articleId;
    /**
     * 用户ID
     */
    private String userId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}

