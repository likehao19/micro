package com.lut.micro.project.entity.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/29 13:31
 */
@Data
public class LikedDTO {
    private Integer commentId;

    private Integer uid;
}
