package com.lut.micro.project.entity;

import com.lut.micro.project.entity.dto.CUserDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

/**
 * (Comments)实体类
 *
 * @author makejava
 * @since 2023-05-25 21:46:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comments implements Serializable {
    private static final long serialVersionUID = 619653979923056362L;
    /**
     * 评论ID
     */
    private String id;
    /**
     * 文章ID
     */
    private int articleId;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 地址
     */
    private String address;
    /**
     * 评论内容
     */
    private String content;
    /**
     * 点赞数
     */
    private Integer likes;
    /**
     * 评论图片
     */
    private String contentImg;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 父评论ID
     */
    private String parentId;
    /**
     *
     */
    private CUserDTO user;
    /**
     *
     */
  /*  private Reply reply;

    @Data
    public static class Reply extends PageInfo<Comments> {

    }*/
}

