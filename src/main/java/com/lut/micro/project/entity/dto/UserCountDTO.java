package com.lut.micro.project.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/02 21:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UserCountDTO {
    private String userId;
    private String nickName;
    private String avatar;
    private int articleCount;
}
