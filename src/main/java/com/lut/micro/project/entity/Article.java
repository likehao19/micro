package com.lut.micro.project.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.io.Serializable;

/**
 * 帖子表(Article)实体类
 *
 * @author makejava
 * @since 2023-05-16 12:01:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Article implements Serializable {
    private static final long serialVersionUID = -87474637882166473L;
    /**
     * 文章ID
     */
    private Integer articleId;
    /**
     * 标题
     */
    private String title;
    /**
     * 发布时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss”,timezone=“GMT+8")
    private String datetime;
    /**
     * 用户
     */
    private String userId;
    /**
     * 内容
     */
    private String content;
    /**
     * 封面
     */
    private String cover;
    /**
     * 类型（1原创 0转载）
     */
    private String type;
    /**
     * 分类
     */
    private Integer categoryId;
    /**
     * 是否允许评论(1 允许 0不允许)
     */
    private String allowComment;
    /**
     * 转载链接
     */
    private String reprintUrl;
    /**
     * 摘要
     */
    private String summary;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 用户头像
     */
    private String avatar;
    /**
     * markdown文本
     */
    private String markdown;
    /**
     * 审核状态
     */
    private String status;
    /**
     * 浏览量
     */
    private int views;
    /**
     * 点赞量
     */
    private int starts;
    /**
     * 评论数
     */
    private int comments;
    /**
     * 是否专题
     */
    private int topic;
    /**
     * 标签
     */
    private String tags;
}

