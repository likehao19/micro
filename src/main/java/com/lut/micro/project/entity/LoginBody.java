package com.lut.micro.project.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/15 11:51
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginBody {
    /**
     * 用户名
     */
    private String userName;
    /**
     * 邮箱
     */
    private String email;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 验证码
     */
    private String code;

    /**
     * 唯一标识
     */
    private String uuid;
}
