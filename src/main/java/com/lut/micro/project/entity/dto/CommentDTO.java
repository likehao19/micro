package com.lut.micro.project.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/29 13:26
 */
@Data
public class CommentDTO {
    private Integer articleId;

    private String content;

    private Integer parentId;

    private MultipartFile[] files;
}
