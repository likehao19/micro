package com.lut.micro.project.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (ArticleCount)实体类
 *
 * @author makejava
 * @since 2023-05-30 18:09:05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ArticleCount implements Serializable {
    private static final long serialVersionUID = -67618842337311785L;
    /**
     * 文章ID
     */
    private Integer articleId;
    /**
     * 浏览量
     */
    private Integer views;
    /**
     * 点赞
     */
    private Integer starts;
    /**
     * 评论数
     */
    private Integer comments;


}

