package com.lut.micro.project.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (UserCount)实体类
 *
 * @author makejava
 * @since 2023-06-02 11:33:46
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UserCount implements Serializable {
    private static final long serialVersionUID = -38055688367711209L;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 关注总数
     */
    private Integer follow;
    /**
     * 点赞总量
     */
    private Integer likes;
    /**
     * 粉丝总数
     */
    private Integer fans;
    /**
     * 文章总计
     */
    private Integer articleCount;



}

