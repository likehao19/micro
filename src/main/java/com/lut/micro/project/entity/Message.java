package com.lut.micro.project.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.io.Serializable;

/**
 * (Message)实体类
 *
 * @author makejava
 * @since 2023-06-10 15:26:26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Message implements Serializable {
    private static final long serialVersionUID = 744246106066910735L;

    private Integer messageId;
    /**
     * 发送者ID
     */
    private String userFromId;
    /**
     * 接受者ID
     */
    private String userToId;
    /**
     * 文本
     */
    private String text;
    /**
     * (0我 1他)
     */
    private String ismine;
    /**
     * 发送者用户头像
     */
    private String avatar;
    /**
     * 发送时间
     */
    @JsonFormat(pattern="HH:mm:ss”,timezone=“GMT+8")
    private String time;

    /**
     * 是否是发送者
     */
    private Boolean isI;

    /**
     * 发送者昵称
     */
    private String nickName;
}

