package com.lut.micro.project.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/28 22:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CUserDTO {
    private String username;
    private String avatar;
    private int level;
    private String homeLink;
}
