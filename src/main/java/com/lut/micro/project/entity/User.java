package com.lut.micro.project.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Date;
import java.io.Serializable;

/**
 * (WblUser)实体类
 *
 * @author makejava
 * @since 2023-05-06 23:19:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class User implements Serializable {
    private static final long serialVersionUID = -85477593492002311L;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 用户类型(00系统用户 11管理员)
     */
    private String userType;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 头像地址
     */
    private String avatar;
    /**
     * 账号状态(0正常 1停用)
     */
    private String status;
    /**
     * 删除标志(0存在 1删除)
     */
    private String delFlag;
    /**
     * 最后登录IP
     */
    private String loginIp;
    /**
     * 最后登录时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss”,timezone=“GMT+8")
    private String loginDate;
    /**
     * 创建者
     */
    private String createBy;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss”,timezone=“GMT+8")
    private String createTime;
    /**
     * 更新者
     */
    private String updateBy;
    /**
     * 更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss”,timezone=“GMT+8")
    private String updateTime;
    /**
     * 个性签名
     */
    private String note;
    /**
     * 备注
     */
    private String remark;
    /**
     * 验证码
     */
    private String code;
    /**
     * 关注
     */
    private Integer follow;
    /**
     * 点赞
     */
    private Integer likes;
    /**
     * 粉丝
     */
    private Integer fans;
    /**
     * 等级
     */
    private Integer level;
    /**
     *
     */
    private String rePassword;

    private Integer articleCount;
}

