package com.lut.micro.project.dao;

import com.lut.micro.project.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/07 18:52
 */
@Mapper
public interface UserMapper {

  int insertUser(User user);

  int selectUserNameUnique(User user);

  int selectEmailUnique(User user);

  User selectUser(User user);

  void insertUserCount(User user);

  int selectPhoneUnique(User user);

  void updateUserInfo(User user);

  int updatePassword(User u);

  User selectUserWithEmail(User user);

  List<User>  selectUserList();

  void updateLoginMsg(User user);

  User selectUserById(String userId);

  List<User> getBanList();

  User getUserInfoById(String userId);

  boolean updateBanStatus(@Param("userId") String userId,@Param("status") String status);

  boolean updateStatus(@Param("userId") String userId,@Param("status") String status);
}
