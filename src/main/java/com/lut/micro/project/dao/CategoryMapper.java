package com.lut.micro.project.dao;

import com.lut.micro.project.entity.Category;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/16 13:27
 */
@Mapper
public interface CategoryMapper {
    List<Category> selectCategoryList();

    void updateCategoryAddCount(Integer categoryId);

    void updateCategorySubCount(Integer categoryId);

    boolean insertCategory(Category category);
}

