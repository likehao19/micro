package com.lut.micro.project.dao;

import com.lut.micro.project.entity.Message;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/10 15:32
 */
@Mapper
public interface MessageMapper {

    List<Message> getMessage(String userFrom, String userTo);

    List<Message> getMessageToMine(String userId);

    void addMessage(Message message);
}
