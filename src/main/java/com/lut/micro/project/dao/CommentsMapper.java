package com.lut.micro.project.dao;

import com.lut.micro.project.entity.Comments;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/25 21:49
 */
@Mapper
public interface CommentsMapper {
    void addComments(Comments comments);

    List<Comments> getComments(String articleId);
}
