package com.lut.micro.project.dao;

import com.lut.micro.project.entity.ArticleCount;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/30 18:37
 */
@Mapper
public interface ArticleCountMapper {

    void updateViews(String articleId);

    void insertArticleCount(Integer articleId);

    void updateStartsAdd(String articleId);

    ArticleCount getArticleCount(String articleId);

    void updateStartsSub(String articleId);

    void deleteArticle(Integer articleId);
}
