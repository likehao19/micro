package com.lut.micro.project.dao;

import com.lut.micro.project.entity.Follow;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/10 21:23
 */
@Mapper
public interface FollowMapper {
    void insertFollow(@Param("userFrom") String userFrom, @Param("userTo") String userTo);

    void deleteFollow(@Param("userFrom") String userFrom, @Param("userTo") String userTo);

    List<Follow> getFollowList(@Param("userId") String userId);

    List<Follow> getFollowedList(@Param("userId")String userId);
}
