package com.lut.micro.project.dao;

import com.lut.micro.project.entity.Article;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/10 23:22
 */
@Mapper
public interface StartsMapper {
    boolean insertStarts(@Param("articleId") String articleId, @Param("userId") String userId);

    boolean deleteStarts(@Param("articleId") String articleId, @Param("userId") String userId);

    List<Article> getStartsList(String userId);
}
