package com.lut.micro.project.dao;

import com.lut.micro.project.entity.UserSetting;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/10 21:54
 */
@Mapper
public interface UserSettingMapper {

    boolean updateUserSetting(UserSetting userSetting);

    boolean insertUserSetting(String userId);

    UserSetting getUserSetting(String userId);

}
