package com.lut.micro.project.dao;

import com.lut.micro.project.entity.dto.UserCountDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/02 11:40
 */
@Mapper
public interface UserCountMapper {

    void updateArticleAddCount(String userId);

    List<UserCountDTO> getUserCountList();

    void updateArticleSubCount(String userId);

    void updateAddFollow(String userFrom);

    void updateSubFollow(String userFrom);

    void updateAddFans(String userTo);

    void updateSubFans(String userTo);
}
