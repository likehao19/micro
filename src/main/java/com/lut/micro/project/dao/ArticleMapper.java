package com.lut.micro.project.dao;

import com.lut.micro.project.entity.Article;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/12 14:51
 */
@Mapper
public interface ArticleMapper {

    Integer insetArticle(Article article);

    List<Article> selectArticleList();

    List<Article> selectArticleListById(String id);

    Article selectArticleById(@Param("articleId") String articleId);

    List<Article> selectTopicList();

    List<Article> getArticle(String userId);

    List<Article> getDrafts(String userId);

    List<Article> getAudit(String userId);

    List<Article> getReviewing(String userId);

    List<Article> getRecycle(String userId);

    boolean insetDraftsArticle(Article article);

    boolean deleteArticle(Article article);

    boolean updateDraftsArticle(Article article);

    List<Article> getAuditList();

    List<Article> getRelatedList(String articleId);

    boolean startReviewArticle(String articleId);

    boolean cancelReview(String articleId);

    void updateArticle(Integer articleId);

    List<Article> selectArticleListByTime(String id);

    List<Article> getHotArticles();

    //审核通过修改状态
    boolean reviewRes(String articleId);

    List<Article> fuzzySearch(@Param("keyword") String keyword);

    boolean realDelArticle(String articleId);
}
