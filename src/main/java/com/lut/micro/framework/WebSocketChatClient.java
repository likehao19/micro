package com.lut.micro.framework;

import javax.websocket.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/07/13 15:51
 */
@ClientEndpoint
public class WebSocketChatClient {
    private static Session session;

    public static void main(String[] args) throws URISyntaxException, IOException, DeploymentException {
        String serverUri = "ws://localhost:9999/chat"; // WebSocket服务器的URI

        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        session = container.connectToServer(WebSocketChatClient.class, new URI(serverUri));

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input;
        while ((input = reader.readLine()) != null) {
            session.getBasicRemote().sendText(input);
        }
    }

    @OnMessage
    public void onMessage(String message) {
        System.out.println("Received message: " + message);
    }
}

