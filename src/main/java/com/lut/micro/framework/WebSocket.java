package com.lut.micro.framework;

import cn.hutool.json.JSONUtil;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import javax.websocket.*;

@ServerEndpoint(value = "/websocket/{userId}")
@Component
public class WebSocket {
    private static ConcurrentHashMap<String, WebSocket> webSocketMap = new ConcurrentHashMap<>();
    private Session session;
    private String userId;

    public static ConcurrentHashMap<String, WebSocket> getWebSocketMap() {
        return webSocketMap;
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("userId") String userId) {
        this.session = session;
        this.userId = userId;
        webSocketMap.put(userId, this);
        sendMessageToAll("用户 " + userId + " 进入聊天室");
        System.out.println("【WebSocket】有新的连接，用户ID：" + userId);
    }

    @OnClose
    public void onClose() {
        webSocketMap.remove(userId);
        sendMessageToAll("用户 " + userId + " 离开聊天室");
        System.out.println("【WebSocket】连接关闭，用户ID：" + userId);
    }

    @OnMessage
    public void onMessage(String message) {
        sendMessageToAll("用户 " + userId + " 说：" + message);
        System.out.println("【WebSocket】收到用户 " + userId + " 的消息：" + message);
    }

    @OnError
    public void onError(Throwable error) {
        System.out.println("【WebSocket】发生错误：" + error.getMessage());
    }

    private static void sendMessageToAll(String message) {
        for (WebSocket webSocket : webSocketMap.values()) {
            try {
                webSocket.session.getBasicRemote().sendText(JSONUtil.toJsonStr(message));
            } catch (IOException e) {
                System.out.println("【WebSocket】向用户 " + webSocket.userId + " 发送消息失败：" + e.getMessage());
            }
        }
    }
}
