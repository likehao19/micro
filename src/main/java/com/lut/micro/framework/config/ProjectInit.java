package com.lut.micro.framework.config;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/11 15:30
 */
@Component
public class ProjectInit {
    @PostConstruct
    public void initProject(){

    }
}
