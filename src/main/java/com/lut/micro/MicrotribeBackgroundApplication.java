package com.lut.micro;

import com.lut.micro.commom.utils.SpringUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class MicrotribeBackgroundApplication {

    public static void main(String[] args) {

        // 解决elasticsearch启动保存问题
        System.setProperty("es.set.netty.runtime.available.processors","false");
        ConfigurableApplicationContext context = SpringApplication.run(MicrotribeBackgroundApplication.class, args);
        SpringUtil.set(context);
      /*  SpringApplication.run(MicrotribeBackgroundApplication.class, args);*/
        System.out.println("(♥◠‿◠)ﾉﾞ  项目启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                "                       __       ___      \n" +
                "                      /\\ \\     /\\_ \\     \n" +
                "  ___     __      ____\\ \\ \\___ \\//\\ \\    \n" +
                " /'___\\ /'__`\\   /',__\\\\ \\  _ `\\ \\ \\ \\   \n" +
                "/\\ \\__//\\ \\L\\.\\_/\\__, `\\\\ \\ \\ \\ \\ \\_\\ \\_ \n" +
                "\\ \\____\\ \\__/.\\_\\/\\____/ \\ \\_\\ \\_\\/\\____\\\n" +
                " \\/____/\\/__/\\/_/\\/___/   \\/_/\\/_/\\/____/");
    }
}
