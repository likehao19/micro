package com.lut.micro.commom.security.interceptor;

import com.alibaba.fastjson.JSON;
import com.lut.micro.commom.redis.RedisUtils;
import com.lut.micro.project.controller.UserController;
import com.lut.micro.project.entity.User;
import com.lut.micro.project.service.IUserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/21 13:36
 */
@Configuration
public class TokenInterceptor implements WebMvcConfigurer {

    private static final Log logger = LogFactory.getLog(TokenInterceptor.class);

    @Autowired
    RedisUtils redisUtils;

    @Autowired
    private IUserService userService;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new HandlerInterceptor() {
            @Override
            public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
                String token=request.getHeader("Authorization");
                if ("root-admin".equals(token)){
                    return true;
                }
                if (token!=null && redisUtils.get("token:"+token)!=null){
                    User u=userService.getUserInfo(token);
                    //重置token和用户信息时间
                    redisUtils.expire("token:"+token,1800);
//                    logger.info("token:"+token+"重置30分钟");
                    redisUtils.expire("userId:"+u.getUserId(), 1800);
//                    logger.info("userId:"+u.getUserId()+"重置30分钟");
                    return true;
                }
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return false;
            }
        }).excludePathPatterns("/user/login","/chat","/analyzer","/userCount","/user/register","/category","/upload","/comments","/user/loginWithEmail","/user","/article","/send-mail/simple{email}");
    }
}
