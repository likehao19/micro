/*
package com.lut.micro.commom.security.filter;

import com.lut.micro.commom.redis.RedisUtils;
import com.lut.micro.commom.security.service.TokenService;
import com.lut.micro.project.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

*/
/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/15 13:34
 *//*

@Component
public class AuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    private TokenService tokenService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException
    {
        RedisUtils redisUtils=new RedisUtils();
        String token = request.getHeader("Authorization");
        if (token!=null && redisUtils.get(token)!=null){
            redisUtils.expire(token,1800);
            chain.doFilter(request, response);
        }else{

        }

    }
}
*/
