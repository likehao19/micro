package com.lut.micro.commom.security.service;

import com.lut.micro.project.entity.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/15 13:37
 */
@Component
public class TokenService {

    // 令牌自定义标识
    @Value("Authorization")
    private String header;

    public User getLoginUser(HttpServletRequest request) {
        String token = request.getHeader(header);
        return null;
    }
}
