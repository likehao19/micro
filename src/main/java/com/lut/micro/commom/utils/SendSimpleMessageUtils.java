package com.lut.micro.commom.utils;

import cn.hutool.extra.spring.SpringUtil;
import com.lut.micro.commom.redis.RedisUtils;
import com.lut.micro.project.entity.MailRequest;


/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/06/11 18:41
 */
public  class SendSimpleMessageUtils {

    /**
     * 邮件内容
     * @param email
     * @param title
     * @param message
     * @return
     */
    public static MailRequest SendSimpleMessage(String email,String title,String message) {
        MailRequest mailRequest=new MailRequest();
        RandomCodeUtils randomCodeUtils=new RandomCodeUtils();
        mailRequest.setSendTo(email);
        mailRequest.setSubject("文章审核结果");
        mailRequest.setText(
                "您的文章"+title+":"+message+"请及时查看！"
        );
        return mailRequest;
    }
}
