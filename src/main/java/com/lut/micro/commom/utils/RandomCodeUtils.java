package com.lut.micro.commom.utils;

import java.util.Random;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/05/07 15:02
 */
public class RandomCodeUtils {
    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    private static final String CHARACTERS_NUM = "1234567890";

    private static final int CODE_LENGTH = 6;
    StringBuilder codeBuilder = new StringBuilder();

    public  String generateRandomCode() {
        Random random = new Random();
        for (int i = 0; i < CODE_LENGTH; i++) {
            int index = random.nextInt(CHARACTERS.length());
            codeBuilder.append(CHARACTERS.charAt(index));
        }
        return codeBuilder.toString();
    }

    public String getRandomCodeWithNum() {
        StringBuilder codeBuilder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < CODE_LENGTH; i++) {
            int index = random.nextInt(CHARACTERS_NUM.length());
            codeBuilder.append(CHARACTERS_NUM.charAt(index));
        }
        return codeBuilder.toString();
    }
}
