package com.lut.micro.commom.utils;

import org.mindrot.jbcrypt.BCrypt;

/**
 * @Auther: Lkh
 * @Description:
 * @Date: 2023/04/25 13:50
 */
public class BcryptUtils {
    private static final int BCRYPT_ROUNDS=12;

    public static String hashPassword(String password) {
        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt(BCRYPT_ROUNDS));
        return hashedPassword;
    }
    public static boolean verifyPassword(String password, String hashedPassword) {
        boolean passwordMatch = BCrypt.checkpw(password, hashedPassword);
        return passwordMatch;
    }
}
